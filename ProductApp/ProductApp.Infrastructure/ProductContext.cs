﻿using ProductApp.Core;
using System.Data.Entity;

namespace ProductApp.Infrastructure
{
    public class ProductContext : DbContext
    {
        public ProductContext() : base("name=ProductAppConnectionString")
        {

        }

        public DbSet Products { get; set; }
    }
}
