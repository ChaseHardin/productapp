using System.Collections;

namespace ProductApp.Core.Interfaces
{
    public interface IProductRepository
    {
        void Add(Product p);
        void Edit(Product p);
        void Remove(int id);
        IEnumerable GetProducts();
        Product FindById(int id);
    }
}
